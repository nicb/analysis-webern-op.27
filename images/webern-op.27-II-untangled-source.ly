% vi:set nowrap:
%
% $Id: webern-op.27-II-untangled-source.mup 1014 2008-01-26 19:07:15Z nicb $
%
% Webern - op.27 row
%
% header
% score
% time=11/1n
% pagewidth=7
% pageheight=2.5
% staffs=1
% stafflines=5
% staff1 clef=treble
% music
% 1: 1b&+g#-; a; a; c#f+; bn--g++; d-e++; f#cn+fn+; c#f#cn+; d-en++; g#-b&+; b--g++;
% invisbar

\version "2.10.33"

\include "_setup.ly"

stemOff = \hide Staff.Stem

\score {
  \new Staff \with { \remove "Time_signature_engraver" \remove "Bar_engraver" }
	{
	  \override Score.MetronomeMark #'transparent = ##t
		\set Staff.midiInstrument = "acoustic grand"
		\key c \major
		\clef treble
	  \time 12/4

    \stemOff <bes'' gis>4 a'4 a'4 <cis' f''>4 <b,! g'''>4 <d e'''>4 <fis' c''! f''!>4 <cis'! fis'! c''!>4 <d e'''!>4 <gis! bes''!>4 <b,! g'''!>4
	}
	\midi {}
  \layout {}
}

