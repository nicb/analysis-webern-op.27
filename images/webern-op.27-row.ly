% vi:set nowrap:
%
% $Id: webern-op.27-row.mup 1014 2008-01-26 19:07:15Z nicb $
%
% Webern - op.27 row
%
\version "2.10.33"

\include "_setup.ly"

stemOff = \hide Staff.Stem

\score {
  \new Staff \with { \remove "Time_signature_engraver" \remove "Bar_engraver" }
	\relative c'' {
	  \override Score.MetronomeMark #'transparent = ##t
		\set Staff.midiInstrument = "acoustic grand"
		\key c \major
		\clef treble
	  \time 12/4

    \stemOff ees4 b4 bes4 d4 cis4 c4 fis,4 e4 g4 f4 a4 gis4
	}
	\midi {}
  \layout {}
}
