#
# $Id: Makefile,v 0.1 2003/12/25 10:03:56 nicb Exp $
#
PRINTMODE=beamer
OUTPUT=slides.pdf
SOURCE=$(OUTPUT:.pdf=.tex)
DVI=$(OUTPUT:.pdf=.dvi)
STRIPPED_OUTPUT=$(OUTPUT:.pdf=)
CP=cp
LATEX=pdflatex
LATEXARG='\newcommand{\printmode}{$(PRINTMODE)}\input{$<}'
DVIPS=dvips
PS2PDF=ps2pdf

$(OUTPUT):	images $(SOURCE)

images:
	$(MAKE) -C images -$(MAKEFLAGS) 

clean:			# clean LaTeX cruft
	$(RM) *.aux *.log *.out *.ps $(DVI) $(OUTPUT) $(DIST_OUTPUT) 
	$(MAKE) -C images -$(MAKEFLAGS) $@

.SUFFIXES: .pdf .tex
.PHONY:	images

%.pdf:	%.tex
	$(LATEX) $(LATEXARG)
	$(RM) $@
	$(LATEX) $(LATEXARG)
	$(RM) $@
	$(LATEX) $(LATEXARG)
